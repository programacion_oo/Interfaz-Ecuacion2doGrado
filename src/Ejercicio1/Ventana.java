package Ejercicio1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField a;
	private JTextField b;
	private JTextField c;
	int x=0;


	public Ventana() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 211);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label = new JLabel("Encontrar X de ecuacion de segundo grado");
		label.setBounds(5, 5, 424, 17);
		label.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		contentPane.add(label);

		JLabel lblNewLabel = new JLabel("X");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		lblNewLabel.setBounds(85, 69, 22, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("X");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		lblNewLabel_1.setBounds(178, 69, 18, 14);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("=");
		lblNewLabel_2.setBounds(269, 69, 18, 14);
		contentPane.add(lblNewLabel_2);

		JLabel label_1 = new JLabel("+");
		label_1.setBounds(103, 69, 12, 14);
		contentPane.add(label_1);

		JLabel label_2 = new JLabel("+");
		label_2.setBounds(196, 69, 18, 14);
		contentPane.add(label_2);

		JLabel label_3 = new JLabel("   2");
		label_3.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		label_3.setBounds(85, 54, 46, 14);
		contentPane.add(label_3);

		a = new JTextField();
		a.setBounds(22, 66, 53, 20);
		contentPane.add(a);
		a.setColumns(10);
		
		b = new JTextField();
		b.setBounds(118, 66, 53, 20);
		contentPane.add(b);
		b.setColumns(10);

		c = new JTextField();
		c.setBounds(206, 66, 53, 20);
		contentPane.add(c);
		c.setColumns(10);
	
		JButton btnConvertir = new JButton("Hallar");
		btnConvertir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(((JButton)e.getSource()).equals(btnConvertir)) {
					if(a.getText().equals("") &&  b.getText().equals("") && c.getText().equals("")) {
						JOptionPane.showMessageDialog(getContentPane(), "Los campos deben de ser completados","Error", JOptionPane.ERROR_MESSAGE);
					}
		
				}
				Convertir con =new Convertir(a,b,c);
				Hallar x= new Hallar(con.getA1(),con.getB1(),con.getC1());
				if(x.getD()<0){
					JOptionPane.showMessageDialog(null,"X1= "+x.getX1()+"i        X2= "+x.getX2()+"i", "hallar x",EXIT_ON_CLOSE);
				}else{
					JOptionPane.showMessageDialog(null,"X1= "+x.getX1()+"   X2= "+x.getX2(), "hallar x",EXIT_ON_CLOSE);
				}
			}
		});





		btnConvertir.setBounds(335, 114, 89, 23);
		contentPane.add(btnConvertir);

		JLabel label_4 = new JLabel("0");
		label_4.setBounds(284, 69, 46, 14);
		contentPane.add(label_4);
	}

}
