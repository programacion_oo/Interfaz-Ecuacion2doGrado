package Ejercicio1;

public class Hallar {

	private double x1;
	 private double x2;
	 private double d;
	public Hallar(double a, double b, double c) {
		d=(Math.pow(b, 2)-(4*a*c));
		x1=(-b+(Math.sqrt(d)/(2*a)));
		x2=(-b-(Math.sqrt(d)/(2*a)));
		if(d<0){
			x1=(-b+(Math.sqrt(-1*d)/(2*a)));
			x2=(-b-(Math.sqrt(-1*d)/(2*a)));
		}
	}
	public double getD() {
		return d;
	}
	public double getX1() {
		return x1;
	}
	public double getX2() {
		return x2;
	}

}