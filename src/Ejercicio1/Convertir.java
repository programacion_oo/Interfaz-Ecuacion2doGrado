package Ejercicio1;

import javax.swing.JTextField;

public class Convertir {
	private double a1;
	private double b1;
	private double c1;

	Convertir(JTextField a, JTextField b, JTextField c){
		String text = a.getText();
		 a1 = Double.parseDouble(text);	

		String text1 = b.getText();
		 b1 = Double.parseDouble(text1);

		String text11 = c.getText();
		 c1 = Double.parseDouble(text11);

	}
	public double getA1() {
		return a1;
	}

	public double getB1() {
		return b1;
	}

	public double getC1() {
		return c1;
	}

}
